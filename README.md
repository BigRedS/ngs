# ngs - Nagios config summary

A bit like `apache2ctl -S`. This one can only deal with our current really-basic
sorts of config because it's not really a parser.

    avi@bigbadwolf:~$ ngs 
    Server patadb.cookie.avi.co
       server_names: patadb.cookie.avi.co
       config at: /etc/nginx/sites-enabled/patadb.cookie.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.106:80
    
    Server stretchbox.avi.co
       server_names: stretchbox.avi.co
       config at: /etc/nginx/sites-enabled/stretchbox.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.107:80
    
    Server merlin.avi.co
       server_names: merlin.avi.co
       config at: /etc/nginx/sites-enabled/merlin.avi.co +8
       listening: 443 80
       backends:
         / -> http://merlin:80
    
    Server grafana.avi.co
       server_names: grafana.avi.co
       config at: /etc/nginx/sites-enabled/grafana.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.102:3000
    
    Server git.avi.co
       server_names: git.avi.co muffinman.avi.co
       config at: /etc/nginx/sites-enabled/git.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.108:80
    
    Server cookie.avi.co
       server_names: cookie.avi.co
       config at: /etc/nginx/sites-enabled/cookie.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.106:80
    
    Server files.avi.co
       server_names: files.avi.co
       config at: /etc/nginx/sites-enabled/files.avi.co +8
       listening: 443 80
       backends:
         / -> http://10.20.30.104:8000
    
